package com.skd.streaming.server;

import java.io.IOException;

import android.content.res.AssetManager;
import android.util.Log;

import com.skd.streaming.DecrypterInputStream;
import com.skd.streaming.server.NanoHTTPD.Response.Status;

public class StreamingServer extends NanoHTTPD {

	private final static String tag = StreamingServer.class.getSimpleName();

	private AssetManager mAssets;
	
	public StreamingServer(AssetManager assets) {
		super(8080);
		this.mAssets = assets;
	}

	@Override
	public Response serve(IHTTPSession session) {
		Log.i(tag, "onServe");
		try {
//			return new Response(Status.PARTIAL_CONTENT, "video/mp4", mAssets.open("splash.mp4"));
			DecrypterInputStream dis = new DecrypterInputStream(mAssets.open("encrypted_splash.mp4"));
			return new Response(Status.PARTIAL_CONTENT, "video/mp4", dis);
		} catch (IOException e) {
			e.printStackTrace();
			return new Response(Status.INTERNAL_ERROR, "text/plain", e.getMessage());
		}
	}
	
	

}
