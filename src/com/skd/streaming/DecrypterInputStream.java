package com.skd.streaming;

import java.io.IOException;
import java.io.InputStream;

public class DecrypterInputStream extends InputStream {
	
	private byte key = 5;
	
	private InputStream mDecoratedStream;
	
	public DecrypterInputStream(InputStream mDecoratedStream) {
		this.mDecoratedStream = mDecoratedStream;
	}

	@Override
	public int read() throws IOException {
		int byteRead = mDecoratedStream.read();
		return byteRead == -1 ? byteRead : byteRead ^ key;
	}

	@Override
	public int read(byte[] buffer, int byteOffset, int byteCount) throws IOException {
		int encCount = mDecoratedStream.read(buffer, byteOffset, byteCount);
		encrypt(buffer);
		return encCount;
	}

	@Override
	public int read(byte[] buffer) throws IOException {
		int encCount = mDecoratedStream.read(buffer);
		encrypt(buffer);
		return encCount;
	}

	private void encrypt(byte[] original){
		for(int i = 0, max = original.length; i < max; i++){
			original[i] = (byte) (original[i] ^ key);
		}
	}

	@Override
	public void close() throws IOException {
		mDecoratedStream.close();
		super.close();
	}

	@Override
	public int available() throws IOException {
		return mDecoratedStream.available();
	}

//	@Override
//	public void mark(int readlimit) {
//		mDecoratedStream.mark(readlimit);
//	}
//
//	@Override
//	public boolean markSupported() {
//		return mDecoratedStream.markSupported();
//	}
//
//	@Override
//	public synchronized void reset() throws IOException {
//		mDecoratedStream.reset();
//	}
//
//	@Override
//	public long skip(long byteCount) throws IOException {
//		return mDecoratedStream.skip(byteCount);
//	}

	
}
