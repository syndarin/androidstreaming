package com.skd.streaming;

import java.io.IOException;

import android.app.Activity;
import android.content.ClipData;
import android.net.Uri;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import com.skd.httpdtest.R;
import com.skd.streaming.server.StreamingServer;

public class StreamingTest extends Activity implements OnClickListener {
	
	private StreamingServer server;

	private final static String VIDEO_PATH = "http://127.0.0.1:8080";

	private VideoView mVideoView;

	private Button mButtonSwitchServer;
	private int bookmark;
	private ProgressBar mProgress;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video_view_test);
		server = new StreamingServer(getAssets());
		mVideoView = (VideoView)findViewById(R.id.videoView);
		mProgress = (ProgressBar)findViewById(R.id.progressBar);
		findViewById(R.id.bPlay).setOnClickListener(this);
		findViewById(R.id.bSetBookmark).setOnClickListener(this);
		findViewById(R.id.bGotoLast).setOnClickListener(this);
		mButtonSwitchServer = (Button) findViewById(R.id.switchServer);
		mButtonSwitchServer.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bPlay:
			mVideoView.setVideoURI(Uri.parse(VIDEO_PATH));
			mVideoView.start();
			break;
		case R.id.bGotoLast:
			gotoBookmark();
			break;
		case R.id.bSetBookmark:
			setBookmark();
			break;
		case R.id.switchServer:
			switchServer();
			break;
		}
	}
	
	private void gotoBookmark(){
		mVideoView.seekTo(207000);
	}
	
	private void setBookmark(){
		bookmark = mVideoView.getCurrentPosition();
	}
	
	private void switchServer(){
		if(server.isAlive()){
			server.stop();
			server.closeAllConnections();
			mButtonSwitchServer.setText("Enable streaming");
		} else {
			try {
				server.start();
				mButtonSwitchServer.setText("Disable streaming");
			} catch (IOException e) {
				e.printStackTrace();
				Toast.makeText(this, "Cannot launch streaming server!", Toast.LENGTH_LONG).show();
			}
		}
	}

}
